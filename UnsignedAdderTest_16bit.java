import org.junit.Assert;
import org.junit.Test;

import static edu.gvsu.dlunit.DLUnit.*;

/**
 * Test cases for an Unsigned, 16-bit adder with a carry-in and overflow.
 * Created by kurmasz on 8/8/16.
 */
public class UnsignedAdderTest_16bit {

  // The complete list of integers to be tests.
  // (You need to add to this list.)
  public static final long testIntegers[] = {0, 1, 2, 13, 127, 128, 129, 0xAAAA, 65534, 65535};

  private void verify(long a, long b, boolean carryIn) {

    long carryInAsInt = (carryIn ? 1 : 0);
    long expected = a + b + carryInAsInt;
    boolean expectedOverflow = expected > 65535;

    setPinUnsigned("InputA", a);
    setPinUnsigned("InputB", b);
    setPin("CarryIn", carryIn);
    run();
    String message = "of " + a + " + " + b + " with " + (carryIn ? "a " : " no ") + " carry in";

    // Output "wraps around" if there is an overflow
    Assert.assertEquals("Output " + message, (expected % 65536), readPinUnsigned("Output"));
    Assert.assertEquals("CarryOut " + message, expectedOverflow, readPin("CarryOut"));
  }

  //
  // Quick tests designed to quickly catch major errors.  (Also serve as example tests)
  //

  @Test
  public void zero_zero_false() {
    verify(0, 0, false);
  }

  @Test
  public void zero_one_false() {
    verify(0, 1, false);
  }

  @Test
  public void zero_zero_true() {
    verify(0, 0, true);
  }

  @Test
  public void zero_one_true() {
    verify(0, 1, true);
  }

  @Test
  public void testCarryInOne(){
  	verify(65535, 0, true);
  }

  @Test
  public void testFirstBitAddition(){
  //Adding Binary 1111_1111_1111_1110
  //with 	  0000_0000_0000_0001
  //Expecting 	  1111_1111_1111_1111
  	verify(65534, 1, false);  
  }

  @Test
  public void testSecondBitAddition(){
  //same principle, but cascading down all the bits
  //from the first test testFirstBitAdditionMethod.
    verify(65533, 2, false);  
  }

  @Test
  public void testThirdBitAdditon(){
     verify(65531, 4, false);
  }

  @Test
  public void testFourthBitAddition(){
    verify(65527, 8, false);
  }

  @Test
  public void testFifthBitAddition(){
    verify(65519, 16, false);
  }

  @Test
  public void testSixthBitAddition(){
    verify(65503,32, false);
  }

  @Test
  public void testSeventhBitAddition(){
    verify(65471, 64, false);
  }

  @Test
  public void testEightBitAddition(){
    verify(65407, 128, false);
  }

  @Test
  public void testNinthBitAddtion(){
    verify(65279, 256, false);
  }

  @Test
  public void test10thBitAddtion(){
     verify(65023, 512, false);
  }

  @Test
  public void test11thBitAddtion(){
    verify(64511, 1024, false);
  }

  @Test
  public void test12thBitAddition(){
    verify(63487, 2048, false);
  }

  @Test
  public void test13thBitAddition(){
    verify(61439, 4096, false);
  }

  @Test
  public void test14thBitAddition(){
    verify(57343, 8192, false);
  }

  @Test
  public void test15thBitAddition(){
    verify(49151, 16384, false);
  }

  @Test
  public void test16thBitAddition(){
    verify(32760,32768, false);
  }
  // This is actually rather gross; but, it is an effective way to thoroughly test your adder without
  // having to write hundreds of individual methods.
  @Test
  public void testAll() {
    for (long a : testIntegers) {
      for (long b : testIntegers) {
        verify(a, b, false);
        verify(a, b, true);
        verify(b, a, false);
        verify(b, a, true);
      }
    }
  } // end testAll
}
